/*!
 * jQuery simple jquery tabs plugin
 * Version: 1.0
 * Author: Vladimir Bujanovic
 * Licensed under the MIT license
 */

(function ($, undefined) {

    // Tabs object
    var vbTabs = {

        options: {
            nav: 'li',
            content: '.tab',
            activeCssClass: 'active',
            animated: true,
            onBeforeChange: $.noop,
            onAfterChange: $.noop
        },

        init: function (options, wrapper) {

            // Mix in the passed-in options with the default options
            this.options = $.extend({}, this.options, options);

            this.$wrapper = $(wrapper);
            this.$nav = this.$wrapper.find(this.options.nav);
            this.$content = this.$wrapper.find(this.options.content);

            if (this.$nav.length === 0 || this.$content.length === 0) {
                return false;
            }

            if (this.options.animated) {
                this.$content.addClass('animated');
            }

            var tabName = window.location.hash.slice(1).length > 0 ?
                          window.location.hash.slice(1) :
                          this.$content.eq(0).attr('data-tab');

            this._getTabNames();
            this._bindEvents();
            this.changeTab(tabName);

            return this;
        },

        _getTabNames: function () {
            var base = this;

            base.tabs = [];
            base.$nav.each(function () {
                base.tabs.push($(this).data('tab'));
            });
        },

        _bindEvents: function () {
            var base = this;

            base.$wrapper.on('click', base.options.nav, function (e) {
                e.preventDefault();

                base.changeTab($(this).data('tab'));
            });
        },

        changeTab: function (tabName) {

            //On before change callback
            this.options.onBeforeChange.call(this, tabName);

            this.$nav.removeClass(this.options.activeCssClass);
            this.$content.removeClass(this.options.activeCssClass);

            if (tabName && $.inArray(tabName, this.tabs) > -1) {
                this.$nav.filter('[data-tab="' + tabName + '"]').addClass(this.options.activeCssClass);
                this.$content.filter('[data-tab="' + tabName + '"]').addClass(this.options.activeCssClass);
            }
            else {
                this.$nav.eq(0).addClass(this.options.activeCssClass);
                this.$content.eq(0).addClass(this.options.activeCssClass);
            }

            window.location.hash = tabName;

            //On after change callback
            this.options.onAfterChange.call(this, tabName);
        }
    };


    // Object.create support test, and fallback for browsers without it
    if (typeof Object.create !== 'function') {
        Object.create = function (o) {
            function F() {
            }

            F.prototype = o;
            return new F();
        };
    }


    // Create a plugin based on a defined object
    $.fn.vbTabs = function (options) {
        return this.each(function () {
            if (!$.data(this, 'vbTabs')) {
                $.data(this, 'vbTabs', Object.create(vbTabs).init(
                    options, this));
            }
        });
    };

}(jQuery, undefined));